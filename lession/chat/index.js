var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

//list User
var users = {};

app.use(express.static('public'));

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function (socket) {

  //new user has joined
  socket.on('newUser', function (newUser) {
    users[newUser.id] = newUser.name;

    socket.broadcast.emit('newUser', newUser);
  });

  //new message has come
  socket.on('message', function (msg) {
    socket.broadcast.emit('newMessage', msg);
  });

  //socket is disconnected
  socket.on('disconnect', function () {
    io.emit('disconnected', users[socket.id]);
    delete users[socket.id];
  });
});

http.listen(process.env.PORT || 5000, function () {
  console.log(`listening on *:${process.env.PORT || 5000}`);
})