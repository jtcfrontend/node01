var User = require('../models/user.model');
var crypto = require('crypto');
var secret = 'meomeomeo';
var path = require('path');
var mail = require('../utils/mail');
var constants = require('../constants');

module.exports = {
    getUsers: getUsers,
    createUser: createUser,
    updateUser: updateUser,
    uploadAvatar: uploadAvatar
}

function uploadAvatar(userId, file) {
    //find user to upload avatar
    return User.findOne({ _id: userId })
        .then(function (user) {
            if (user) {
                return new Promise(function (resolve, reject) {
                    //move to avatar folder
                    file.mv(path.join(__dirname, '../public/avatar/avatar_' + user._id + '.png'), function (err) {
                        if (err)
                            reject(err);

                        //update current user with new avatar path
                        return User.findOneAndUpdate({ _id: userId }, { $set: { avatar: 'avatar_' + user._id + '.png' } })
                            .then(function (data) {
                                resolve(`${constants.imageDomain}/avatar/avatar_${user._id}.png`);
                            })
                            .then(function (err) {
                                reject(err);
                            })
                    });
                });

            } else {
                return Promise.reject({
                    message: "Not Found",
                    statusCode: 404
                });
            }
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}

function updateUser(user) {
    return User.findByIdAndUpdate(user._id, user)
        .then(function (user) {
            return Promise.resolve(user);
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}

function getUsers() {
    return User.find({}, { password: 0 })
        .populate({
            path: 'books',
            select: 'title -_id',
            populate: {
                path: 'author',
                select: 'name email -_id'
            }
        })
        .then(function (users) {
            return Promise.resolve(users);
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}

function createUser(newUser) {
    return User.find({ email: newUser.email })
        .then(function (foundUsers) {
            if (foundUsers.length > 0) {
                return Promise.reject({
                    statusCode: 400,
                    message: 'Email is existed'
                });
            } else {
                var hash = crypto.createHmac('sha256', secret)
                    .update(newUser.password)
                    .digest('hex');

                newUser.password = hash;
                var user = new User(newUser);

                return user.save()
                    .then(function (user) {
                        return mail.sendMail('', user.email, 'New user registration', '<h1>Wellcome to the APP</h1>')
                            .then(function (res) {
                                return Promise.resolve(res);
                            })
                            .catch(function (err) {
                                return Promise.reject(err);
                            })
                    })
                    .catch(function (err) {
                        return Promise.reject(err);
                    })
            }
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}

