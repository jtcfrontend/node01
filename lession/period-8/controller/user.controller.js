var fs = require('fs');
var path = require('path');

module.exports = {
    getUsers: getUsers
}

function getUsers() {
    return new Promise(function (resolve, reject) {
        fs.readFile(path.join(__dirname, "../" + "users.json"), 'utf8', function (err, data) {
            if (err) {
                reject({
                    message: err.message
                });
            } else {
                resolve(data);
            }
        });
    })
}

function createUser(newUser) {
    return new Promise(function (resolve, reject) {
        fs.readFile(path.join(__dirname, "../" + "users.json"), 'utf8', function (err, data) {
            var list = JSON.parse(data);
            list.push(newUser);

            fs.writeFile(path.join(__dirname, "../" + "users.json"), JSON.stringify(list), function (err, data) {
                if (err) {
                    reject({
                        message: err.message
                    });
                } else {
                    resolve(data);
                }
            });
        });
    });
}