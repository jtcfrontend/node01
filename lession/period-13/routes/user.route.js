var router = require('express').Router();
var fs = require('fs');
var path = require('path');
var auth = require('../middle-ware/auth');
var userController = require('../controller/user.controller');

/**
 * @api {get} /users Get Users
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiSuccess {String} email Email of the User.
 * @apiSuccess {String} name Name of the User.
 * @apiSuccess {String} profession Profession of the User.
 * @apiSuccess {Array} books List of books of the User.
 * @apiSuccess {String} avatar Avatar of the User.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 * [
 *    {
 *        "profession": "sinhvien",
 *        "books": [],
 *        "_id": "5a702b259bdd1c3240acba7b",
 *        "name": "huong",
 *        "__v": 0,
 *        "avatar": "avatar_5a702b259bdd1c3240acba7b.png"
 *    }
 *    {
 *        "profession": "sinhvien",
 *        "books": [
 *            {
 *                "author": {
 *                    "email": "ha@ha.com",
 *                    "name": "ahihi"
 *                },
 *                "title": "Beginner's History of America"
 *            }
 *        ],
 *        "_id": "5a7290ca55d3e7259c083b5b",
 *        "email": "ha@ha.com",
 *        "name": "ahihi",
 *        "__v": 1,
 *        "avatar": "avatar_5a7290ca55d3e7259c083b5b.png"
 *    }
 * ]
 *
 * @apiError UserNotFound The the User was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "UserNotFound"
 *     }
 */
router.get('/', auth.auth(), getUsers);

router.post('/', createUser);

/**
 * @api {get} /users/:id Request User
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {Number} id Users unique ID.
 *
 * @apiSuccess {String} email Email of the User.
 * @apiSuccess {String} name Name of the User.
 * @apiSuccess {String} profession Profession of the User.
 * @apiSuccess {Array} books List of books of the User.
 * @apiSuccess {String} avatar Avatar of the User.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *    {
 *        "profession": "sinhvien",
 *        "books": [
 *            {
 *                "author": {
 *                    "email": "ha@ha.com",
 *                    "name": "ahihi"
 *                },
 *                "title": "Beginner's History of America"
 *            }
 *        ],
 *        "_id": "5a7290ca55d3e7259c083b5b",
 *        "email": "ha@ha.com",
 *        "name": "ahihi",
 *        "__v": 1,
 *        "avatar": "avatar_5a7290ca55d3e7259c083b5b.png"
 *    }
 *
 * @apiError UserNotFound The id of the User was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "UserNotFound"
 *     }
 */
router.put('/:id', auth.auth(), updateUser);
router.post('/avatar', auth.auth(), uploadAvatar);

module.exports = router;

function uploadAvatar(req, res, next) {
    if (!req.files)
        return next({
            message: 'No files were uploaded.'
        });

    var uploadedFile = req.files.file;

    userController.uploadAvatar(req.user._id, uploadedFile)
        .then(function (avatar) {
            res.send({
                avatar: avatar
            })
        })
        .catch(function (err) {
            next(err);
        })
}

function updateUser(req, res, next) {
    var id = req.params.id;
    var user = req.body;
    user._id = id;
    userController.updateUser(user)
        .then(function (user) {
            res.send(user);
        })
        .catch(function (err) {
            next(err);
        })
}

function getUsers(req, res, next) {
    userController.getUsers()
        .then(function (users) {
            res.send(users);
        })
        .catch(function (err) {
            next(err);
        })
}

function createUser(req, res, next) {
    var newUser = req.body;
    if (!newUser.email) {
        next({
            statusCode: 400,
            message: "Email is required"
        })
    } else if (!newUser.password) {
        next({
            statusCode: 400,
            message: "Password is required"
        })
    } if (!newUser.name) {
        next({
            statusCode: 400,
            message: "Name is required"
        })
    } else {
        userController.createUser(newUser)
            .then(function (user) {
                res.json(user);
            })
            .catch(function (err) {
                next(err);
            })
    }
}