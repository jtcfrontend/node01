var express = require('express');
var app = express();
var fs = require("fs");
var bodyParser = require('body-parser');
var router = express.Router();
var userRouter = require('./routes/user.route');
var authRouter = require('./routes/auth.route');
var bookRouter = require('./routes/book.route');
var errorHandler = require('./middle-ware/error-handler');
var db = require('./db');
var fileUpload = require('express-fileupload');
var constants = require('./constants');

var allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, x-access-token');

    next();
};
app.use(allowCrossDomain);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('public'));
app.use(express.static('apidoc'));
app.use(fileUpload());

app.use('/users', userRouter);
app.use('/auth', authRouter);
app.use('/books', bookRouter);

app.use(errorHandler.errorHandler());

app.listen(constants.server.port, function () {
    console.log(`Ung dung Node.js dang lang nghe tai dia chi: ${constants.server.domain}:${constants.server.port}`);
})