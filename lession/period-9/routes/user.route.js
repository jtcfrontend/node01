var router = require('express').Router();
var fs = require("fs");
var path = require('path');
var auth = require('../middle-ware/auth.js');
var userController = require('../controller/user.controller');

module.exports = function () {
    router.get('/', auth.auth(), getUsers);
    router.post('/', createUser);

    return router;
};

function getUsers(req, res, next) {
    userController.getUsers().then(function(users) {
        res.send(users);
    }, function(err) {
        next(err);
    })
}

function createUser(req, res, next) {
    var newUser = req.body;
    userController.createUser(newUser).then(function(users) {
        res.send(users);
    }, function(err) {
        next(err);
    })
}